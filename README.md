# Welcome!

Never have I been able to find a note-taking mechanism that works for me. Hand-writing? Well, I've heard all about the difference in remembering written-versus-typed things, but my OCD stops me from actually paying attention to my notes. I write so terse to save time and paper, I'm not actually coming to a deeper understanding of the content, let alone processing what was just said. Typing? They're just as terse, and there are so many different places to put my notes, I'm never in one place for very long. Plus, whenever I want to go back to my notes, they're hard to get to, no matter how many apps I have to tie them to my phone or wherever.

The solution? This book.

This book is my attempt to create a better way \(for me\) to take notes. It's mainly for me, but it's a little bit for you; I'm writing this GitBook in an attempt to open-source the knowledge that I pick up through lectures and various seminars. Why does the content need to be repeated so many times when it could be free on the internet? That said, I don't want you to steal my notes  or plagiarize my work, I just want to have a public place to write and keep my notes.

"How does this solve your problem?" You might ask. Well, if I'm keeping a public notebook, the idea is that I need to write my notes in a form that can be understood by others. If I can translate the content taught to me into a format that can be read and utilized by others, then I believe that I will retain the information better. It's the same idea that if you want to truly learn something, you have to teach it to others.

---

## What to Expect

You won't find the secrets to the universe here. I'm a business major at Oregon State University, so if you're in the same vein as me, this book could help. However, I already have a lot of the Baccalaureate core out of the way, so there are 45 credits worth of notes that won't make it onto the pages of this book. However, I have yet to take the great majority of my Pre-Business core requirements, so you should get some good notes from that.

### Writing Style

In terms of note-taking style, my notes are mainly bulleted. I notice, however, that GitBook has a handy little math format, so that will come in handy when I get around to taking MTH 241. I'll try to write TL;DRs where I have the time, but I wouldn't rely on them, if I were you.

### Written Notes

I'm aware that some professors aren't the biggest fans of computers in the classroom. It's already a struggle that I run into all the time. So, I'm going to make an attempt to transcribe my written notes into this book. I won't submit you to pictures of my handwriting, I'm not into getting booked for crimes against humanity.

### Language Classes

I'm in JPN 112 right now, and I don't want to take the effort to teach a language. No thanks. Try somewhere else.

---

## Plagiarism

I said it already, but I'll say it again. **These notes are not for you to copy.** I don't really need to go after you if you copy these notes because I'm sure your university already has hell in store for you, but do yourself a favor and save yourself the trouble. Use these notes to supplement your learning or verify your notes.

---

## Contact

If you want to get in touch with me, you can do it on Twitter @Monochromicom or at my OSU email: barberg \[at\] oregonstate \[dot\] edu.



