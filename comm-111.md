# Public Speaking

Ah, a classic. This course is one of the Bacc Core courses. The instructor tells me that the midterm and finals are based on the lecture sections, so I'll be indicating that **by bolding them**. Unfortunately, this term, we've already missed 2 lectures,  so we're already at Week 3. Let's get started.

## Class 3, Week 3

* When you prepare your speech, make sure you prepare the body first. In order:
  * Outline your main points of the speech.
    * Think of your logical divisions, reasons, and specific steps.
    * If you don't have your main points first, the rest of the speech doesn't have meaning.
  * Think about how you're going to organize your points.
    * Organize them
      * Chronologically, or in order of occurance.
      * Topically
      * Spatially
      * Cause and Effect
      * Problem/solution
  * Think about how you're going to link your main points together.
    * **There are 4 different types of connectives:**
      * Transitions, which indicate that you've finished one thought and are going to move on to another.

      * Internal previews indicate what is going to be spoken about next.

      * Internal summaries, which hash what the speaker has already discussed, to lead into the next topic.
      * Signposts, which are brief statements that indicate either where the speaker is, or signals what the main points are.
* Next, prepare the introduction, because it's important to start your speech off right.
  * **The Introduction has 4 different purposes: **

* **It must get the audience's attention.**
  * There are 8 ways to get the audience's attention.
    * Anecdotes, or short stories.
    * Startling statements, which do just about what you think they do.
    * Quotes, which needs to be related and actually interesting.
    * **Humor**, allows the audience to connect to you.
    * Question, which are the easiest to implement.
    * Personal experience, which is sort of like an anecdote, but you're the main character.
    * Reference to recent events.
    * Activity, or making the audience do something.
* **It must establish credibility.**
  * Make it look like you care about the audience, and that you have the audience's goodwill in mind.
* **It must relate the audience to the topic.**
  * Relates to the topic above.
* **It must give a preview as to what you will discuss.**
  * If you lack a preview, you don't have a speech.
  * Make sure that you say what your speech is going to talk about.

* Finally, prepare the conclusion.
  * **The conclusion needs to do another 4 things:**
    * Signal the end of the speech.
    * Reinforce and summarize the central idea.
    * Motivate the audience, or give a call to action.
    * Have a memorable ending. Here are 4 different ways:
      * Quotes
      * Humor
      * Anecdotes
      * Questions
      * Reference introduction
      * Appeal to action



